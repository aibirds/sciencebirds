﻿using UnityEditor;
using System.IO;
public class CreateAssetBundles
{
    [MenuItem("Assets/Build AssetBundles")]
    static void BuildAllAssetBundles()
    {
        string assetBundleDirectory = "Assets/AssetBundles";
        if(!Directory.Exists(assetBundleDirectory)) {
            Directory.CreateDirectory(assetBundleDirectory);
        }
        BuildPipeline.BuildAssetBundles(assetBundleDirectory, 
                                        BuildAssetBundleOptions.UncompressedAssetBundle, 
                                        EditorUserBuildSettings.activeBuildTarget);
                                        //BuildTarget.StandaloneLinux64);
    }
}


// public class CreateAssetBundles
// {

//     // command to run the novelty genertor
//     // "pathToUnityExecutable"  -projectPath "pathOfTheProject"  -quit -batchmode -executeMethod CreateAssetBundle.build
//     // eg: "C:\Program Files\Unity\Hub\Editor\2019.2.17f1\Editor\Unity.exe"  -projectPath "E:\OneDrive - Australian National University\20-02\sciencebirds-novelty-test" -quit -batchmode -executeMethod CreateAssetBundle.build



//     // the path to save the asset bundle
//     private static string pathToSaveAssetBundle = "Assets/Resources/Novelty/GeneratedAssetBundle/";

//     [MenuItem("Novelty Generator/Generate Asset Bundle")]
//     public static void build()
//     {

//         // creating an instance of novelty generator and generating novelty
//         NoveltyGenerator noveltyGenerator = ScriptableObject.CreateInstance("NoveltyGenerator") as NoveltyGenerator;

//         // create new prefabs or use existing
//         if (noveltyGenerator.createNovelPrefabs)
//         {
//             PrefabsGenerator.generateBatchPrefabs(noveltyGenerator.noOfPrefabsToGenerate, 0, NovelObjectType.pig);
//             PrefabsGenerator.generateBatchPrefabs(noveltyGenerator.noOfPrefabsToGenerate, noveltyGenerator.noOfPrefabsToGenerate, NovelObjectType.bird);
//             PrefabsGenerator.generateBatchPrefabs(noveltyGenerator.noOfPrefabsToGenerate, 2 * noveltyGenerator.noOfPrefabsToGenerate, NovelObjectType.block);

//             // wait few seconds to update the directory
//             System.Threading.Thread.Sleep(5000);
//         }


//         noveltyGenerator.generateNovelty();
//         Debug.Log("NoveltyLevel: " + noveltyGenerator.noveltyLevel);

//         string[] configuredAssests = null;

//         if (noveltyGenerator.noveltyLevel == 2 | noveltyGenerator.noveltyLevel == 1)
//         {

//             // reading the configured prefabs and materials and getting the individual paths
//             FileInfo[] configuredPrefabs = new DirectoryInfo(noveltyGenerator.pathToSaveConfiguredAssests).GetFiles("*.prefab");
//             FileInfo[] configuredMaterials = new DirectoryInfo(noveltyGenerator.pathToSaveConfiguredAssests).GetFiles("*.physicsMaterial2D");

//             configuredAssests = new string[configuredPrefabs.Length + configuredMaterials.Length];

//             for (int i = 0; i < configuredPrefabs.Length; i++)
//             {
//                 configuredAssests[i] = noveltyGenerator.pathToSaveConfiguredAssests + "/" + configuredPrefabs[i].Name;
//             }
//             for (int i = configuredPrefabs.Length; i < configuredPrefabs.Length + configuredMaterials.Length; i++)
//             {
//                 configuredAssests[i] = noveltyGenerator.pathToSaveConfiguredAssests + "/" + configuredMaterials[i - configuredPrefabs.Length].Name;
//             }

//         }
//         else if (noveltyGenerator.noveltyLevel == 3)
//         {
//             configuredAssests = new string[] { NoveltyGenerator.noveltyGenerationBaseDirectory + "/" + noveltyGenerator.level3ConfigFileName };
//             //configuredAssests = new string[2];
//             //configuredAssests[0] = NoveltyGenerator.noveltyGenerationBaseDirectory + "/" + noveltyGenerator.level3ConfigFileName;
//             //configuredAssests[1] = "Assets/Resources/Novelty/NovelPrefabs/novel_object_1.prefab";
//         }


//         // creating the asset bundle using the configured prefabs and building the asset bundle
//         AssetBundleBuild[] buildMap = new AssetBundleBuild[1];
//         buildMap[0].assetBundleName = "AssetBundle";
//         buildMap[0].assetNames = configuredAssests;

//         // build the asset bundle
//         switch (noveltyGenerator.operatingSystem)
//         {
//             case "StandaloneLinux":
//                 BuildPipeline.BuildAssetBundles(pathToSaveAssetBundle + "linux", buildMap, BuildAssetBundleOptions.None, BuildTarget.StandaloneLinux64);
//                 break;
//             case "StandaloneOSX":
//                 BuildPipeline.BuildAssetBundles(pathToSaveAssetBundle + "OSX", buildMap, BuildAssetBundleOptions.None, BuildTarget.StandaloneOSX);
//                 break;
//             case "StandaloneWindows":
//                 BuildPipeline.BuildAssetBundles(pathToSaveAssetBundle + "windows", buildMap, BuildAssetBundleOptions.None, BuildTarget.StandaloneWindows);
//                 break;
//             case "All":
//                 buildMap[0].assetBundleName = "AssetBundle";
//                 BuildPipeline.BuildAssetBundles(pathToSaveAssetBundle + "linux", buildMap, BuildAssetBundleOptions.None, BuildTarget.StandaloneLinux64);
//                 buildMap[0].assetBundleName = "AssetBundle";
//                 BuildPipeline.BuildAssetBundles(pathToSaveAssetBundle + "OSX", buildMap, BuildAssetBundleOptions.None, BuildTarget.StandaloneOSX);
//                 buildMap[0].assetBundleName = "AssetBundle";
//                 BuildPipeline.BuildAssetBundles(pathToSaveAssetBundle + "windows", buildMap, BuildAssetBundleOptions.None, BuildTarget.StandaloneWindows);
//                 break;
//             default:
//                 Debug.Log("OS undefined for building the asset bundle!");
//                 break;
//         }

//     }
// }
